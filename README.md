
# RedLight Summer Internship 2023 - Design Challenge

![banner](/assets/banner.png)

#

In this challenge you should **design** and **implement** a **landing page** for a typical "tasco" of Coimbra 🍗.

Your tasco is a small restaurant in the heart of Coimbra that serves authentic regional dishes. It should be known for its loud staff and low prices. You can find inspiration in tascos like "Quim dos Ossos", "Casa Chelense", "Zé Manel dos Ossos" and "Taberna Casa Costa". 

The aim of this challenge is to build a simple web page that promotes the restaurant. You should design an appealing layout and add some animations if you wish, in order to impress future clients 🤑.

#

### Goals

In this page we want to find:

- A short description about the tasco and its history;
- Top 3 dishes;
- The restaurant crew;
- An image gallery (optional - only for masters 😎);
- Contacts and location.

Again, we're looking for something simple, creative an well structured.
You can use images of your choice and "Lorem ipsum" whenever you want. Colors and typography are also choosen by you.
Don't overthink 🤔.

#

### Phases

##### 1.Research 🔎
Research and show us similar solutions that you find inspirational. This step is very important to us, don't skip it.

##### 2.Design 🎨
Design a high-fidelity mockup for your proposal. You can use any tool you want such as Figma, Adobe XD, Sketch, etc.

##### 3.Build 💻
Implement the designed page using HTML and CSS (You can also use SCSS, SASS, LESS or other type of styling).
If you're a legend, you can also add some JavaScript for animations or any dynamic content you want to do.

#

### Delivery
When you're done, you should fork this repository and upload your work there to share it with us or you can simply send everything in a .zip folder or a WeTransfer link.
Please try to share your process going through the steps you took to reach your final version.
